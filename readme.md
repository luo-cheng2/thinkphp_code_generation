#thinkphp6 批量生成模型

引用方式
``
composer require luocheng/thinkphp_code_generation
``

批量生成model
``
php think batch:make:model
``
执行脚本过后将会创建所有的模型，并添加上相关的注释

批量生成controller
``
php think batch:make:controller
``
