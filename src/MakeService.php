<?php

namespace think\generation;

use think\generation\command\BatchMakeController;
use think\generation\command\BatchMakeModel;
use think\Service;

class MakeService extends Service
{
    public function boot()
    {
        $this->commands([
            'batch:make:model' => BatchMakeModel::class,
            'batch:make:controller' => BatchMakeController::class,
        ]);
    }
}
