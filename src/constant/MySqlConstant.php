<?php

namespace think\generation\constant;

class MySqlConstant
{
    /**
     * 数据库字段对应的，php类型映射
     */
    const FIELD_MAPPING = [
        'bigint' => 'int',
        'date' => 'string',
        'datetime' => 'string',
        'decimal' => 'float',
        'double' => 'float',
        'float' => 'float',
        'int' => 'int',
        'integer' => 'int',
        'json' => 'string',
        'mediumint' => 'int',
        'mediumtext' => 'string',
        'text' => 'string',
        'tinyint' => 'int',
        'tinytext' => 'string',
        'varchar' => 'string',
    ];
}
