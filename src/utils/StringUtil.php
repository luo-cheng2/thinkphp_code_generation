<?php

namespace think\generation\utils;

class StringUtil
{
    /**
     * 提取出数据库中的字段类型
     *
     * @param string $field
     * @return string
     */
    public static function getFieldType(string $field): string
    {
        $pattern = '/\(.*?\)/';
        $field = preg_replace($pattern, '', $field);
        $field = str_replace('unsigned', '', $field);
        return trim($field);
    }
}
