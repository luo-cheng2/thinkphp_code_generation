<?php

declare(strict_types=1);

namespace think\generation\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\facade\Db;
use think\helper\Str;

class BatchMakeController extends Command
{
    private array $skipField = ['create_time', 'update_time', 'delete_time'];
    private array $fieldList = [];

    protected function configure()
    {
        // 指令配置
        $this->setName('batch:make:controller')
            ->addArgument("tableName", Argument::OPTIONAL, '表名称 或 all', 'all')
            ->setDescription('批量创建Controller');
    }

    protected function getStub()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'controller.stub';
    }

    protected function execute(Input $input, Output $output)
    {
        $tableName = $input->getArgument("tableName");
        if ($tableName == "all") {
            $tables = Db::getTables();

            foreach ($tables as $itemTableName) {
                $this->makeFile($itemTableName);
            }
        } else {
            $this->makeFile($tableName);
        }

    }

    protected function makeFile($tableName)
    {
        $this->initFieldList($tableName);

        $output = $this->output;
        $classname = $this->getClassName($this->formatClassName($tableName));
        $pathname = $this->getPathName($classname);

        if (is_file($pathname)) {
            $output->writeln('<error>' . $classname . ' already exists!</error>');
        }

        if (!is_dir(dirname($pathname))) {
            mkdir(dirname($pathname), 0755, true);
        }

        file_put_contents($pathname, $this->buildClass($tableName, $classname));

        $output->writeln('<info>' . $classname . ' created successfully.</info>');
        $this->fieldList = [];
    }

    protected function initFieldList($tableName)
    {
        if (strpos($tableName, '@')) {
            [$app, $tableName] = explode('@', $tableName);
        }

        $this->fieldList = Db::getFields($tableName);
    }

    protected function buildClass(string $tableName, string $className)
    {
        $stub = file_get_contents($this->getStub());

        $namespace = trim(implode('\\', array_slice(explode('\\', $className), 0, -1)), '\\');

        $class = str_replace($namespace . '\\', '', $className);
        $formatClassName = $this->formatClassName($tableName);
        return str_replace(
            [
                '{%className%}',
                '{%namespace%}',
                '{%useModel%}',
                '{%Model%}',
                '{%inputField%}',
                '{%updateField%}',
            ],
            [
                $class,
                $namespace,
                'use app\model\\' . $formatClassName . 'Model;',
                $formatClassName . 'Model',
                $this->getInputField(),
                $this->getUpdateField()
            ],
            $stub
        );
    }


    protected function getInputField(): string
    {
        $returnData = '';

        foreach ($this->fieldList as $value) {
            if (in_array($value['name'], $this->skipField)) {
                continue;
            }
            $returnData .= '$' . $value['name'] . ' = $this->request->param(\'' . $value['name'] . '\');' . PHP_EOL;
        }
        return $returnData;
    }

    protected function getUpdateField(): string
    {
        $returnData = '';
        foreach ($this->fieldList as $value) {
            if (in_array($value['name'], array_merge($this->skipField, ['id']))) {
                continue;
            }
            $returnData .= '$data->' . $value['name'] . ' = $' . $value['name'] . ';' . PHP_EOL;
        }
        return $returnData;
    }

    protected function formatClassName($tableName)
    {
        $prefix = env("DATABASE.prefix");
        if ($prefix && strpos($tableName, $prefix) === 0) {
            $tableName = substr($tableName, strlen($prefix));
        }
        return Str::studly($tableName);
    }

    protected function getClassName(string $name): string
    {
        if (strpos($name, '\\') !== false) {
            return $name . ($this->app->config->get('route.controller_suffix') ? 'Controller' : '');
        }

        if (strpos($name, '@')) {
            [$app, $name] = explode('@', $name);
        } else {
            $app = '';
        }

        if (strpos($name, '/') !== false) {
            $name = str_replace('/', '\\', $name);
        }

        return $this->getNamespace($app) . '\\' . $name . ($this->app->config->get('route.controller_suffix') ? 'Controller' : '');
    }

    protected function getNamespace(string $app): string
    {
        return 'app' . ($app ? '\\' . $app : '') . '\\controller';
    }

    protected function getPathName(string $name): string
    {
        $name = substr($name, 4);

        return $this->app->getBasePath() . ltrim(str_replace('\\', '/', $name), '/') . '.php';
    }

}
